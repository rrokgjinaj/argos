-- Put your global variables here

best_angle = -1 -- highest value found so far
linefound = false
sensors={}
soglia = 0.5
speed = 2

--[[ This function is executed every time you press the 'execute'
     button ]]
function init()
	speed= 7--math.random( 2, 5 )
	linefound=false
	left_v = 10
	right_v = 10
	robot.wheels.set_velocity(left_v,right_v)
	robot.leds.set_all_colors("black")
end
--[[ This function is executed at each time step
     It must contain the logic of your controller ]]
function step()
	robot.wheels.set_velocity(left_v,right_v)
	left_v=speed
	right_v=speed
	-- Check if on linefound
	if not linefound then
		findLine()
	else 
		followLine()
		obstacleAvoidance()
	end
end

function findLine()
	for i=1,8 do
		if robot.base_ground[i].value == 0 then
			linefound = true
			left_v=0
			right_v=0
			break
		end
	end
	left_v=speed
	right_v=speed
	for i=1,24 do
		if robot.proximity[i].value>soglia  then
			if i<12 then
				left_v=speed
				right_v=0
			else 
				left_v=0
				right_v=speed
			end
		end
	end
	-- if linefound == true then
	-- 	robot.leds.set_all_colors("red")
	-- else
	-- 	robot.leds.set_all_colors("black")
	-- end
end

function followLine()
	if robot.base_ground[2].value==0 and robot.base_ground[8].value==1 then
		--log("follow go left")
		left_v=0
		right_v=5
	elseif robot.base_ground[2].value==1 and robot.base_ground[8].value==0 then
		--log("follow go right")
		left_v=5
		right_v=0
	elseif robot.base_ground[1].value==1 then
		--log("follow go someware")
		left_v=5
		right_v=0
	else
		--log("follow go drit")
		left_v=speed
		right_v=speed

	end
end
avoiding_left_obstacle=false
function obstacleAvoidance()

	if robot.base_ground[1].value==0 and avoiding_left_obstacle then
		left_v=2
		right_v=-2
		log("escape")
		avoiding_left_obstacle=false
	end

	for i=1,24 do
		if robot.proximity[i].value>soglia  then
			if i<5 or i>13 then
				log("go right")
				left_v=2
				right_v=-2
				avoiding_left_obstacle=true
				return
			elseif i>=6 and i <= 7 and avoiding_left_obstacle then
				log("go drit avoiding")
				left_v=3
				right_v=3
				return
			end
		end
	end
	if avoiding_left_obstacle then
		log("go left")
		right_v=2
		left_v=0
	end


	-- if  (robot.base_ground[1].value == 0 or robot.base_ground[2].value == 0 or robot.base_ground[7].value == 0) then
	-- 	if avoiding_right_obstacle then
	-- 		left_v=10
	-- 		right_v=0
	-- 		avoiding_right_obstacle=false
	-- 		log("avoiding_right_obstacle then line")
	-- 	elseif avoiding_left_obstacle then
	-- 		left_v=0
	-- 		right_v=10
	-- 		avoiding_left_obstacle=false
	-- 		log("avoiding_left_obstacle then line")
	-- 	end
	-- else if not martellata then
	-- 	if avoiding_right_obstacle then
	-- 		log("no obstacle,avoinding right")
	-- 		left_v=5
	-- 		right_v=1
	-- 	elseif avoiding_left_obstacle then
	-- 		log("no obstacle,avoinding left")
	-- 		left_v=1
	-- 		right_v=5				
	-- 	end
	-- 	end
	-- end
end
--[[ This function is executed every time you press the 'reset'
     button in the GUI. It is supposed to restore the state
     of the controller to whatever it was right after init() was
     called. The state of sensors and actuators is reset
     automatically by ARGoS. ]]
function reset()
	init()
end



--[[ This function is executed only once, when the robot is removed
     from the simulation ]]
function destroy()
   -- put your code here
end
